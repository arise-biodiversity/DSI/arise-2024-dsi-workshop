# ARISE 2024 DSI workshop


## Introduction

This public repository contains files for the Digital Species Identification (DSI) workshop that is organised as part of the [ARISE Day 2024](https://www.arise-biodiversity.nl/arise-day-2024) on Wednesday March 27th.

In the first part of the workshop, the ARISE DSI system is used to prepare datasets with photos from the Diopsis insect cameras. The datasets are analysed by a specialised AI algorithm on the Snellius supercomputer.

During the second part of the workshop, we will look at analysis results of Diopsis photos. The analysis results are stored in a CSV file that we will process and visualise with a Jupyter Notebook.

## License
Apache License Version 2.0.
